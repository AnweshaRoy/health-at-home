# ----------------------------------------------------------------------------
# Search utility stuff, filtering/ordering functions
# ----------------------------------------------------------------------------

from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from sqlalchemy import Column, String, Integer, cast
from sqlalchemy.dialects.postgresql import ARRAY
from flask_cors import CORS
import requests
import time
import urllib
import os
import json
from sqlalchemy import create_engine
from sqlalchemy.sql import text
import flask_restless
import re

import init
import models

# bring environment variables, model definitions, ... into our namespace
# because those prefixes are annoying
Recipe = models.Recipe
recipe_schema = models.recipe_schema
recipes_schema = models.recipes_schema
Workout = models.Workout
workout_schema = models.workout_schema
workouts_schema = models.workouts_schema
Article = models.Article
article_schema = models.article_schema
articles_schema = models.articles_schema
GenericError = models.GenericError

# Dictionaries to help convert our source's provided information (ints)
# turns them into strings (user- and search-friendly)
workoutLanguages = {
    "1": "Deutsch",
    "2": "English",
    "3": "български език",
    "4": "Español",
    "5": "Русский",
    "6": "Nederlands",
    "7": "Português",
    "8": "ελληνικά",
    "9": "čeština",
    "10": "Svenska",
    "11": "Norsk",
}
workoutCategories = {
    "8": "Arms",
    "9": "Legs",
    "10": "Abs",
    "11": "Chest",
    "12": "Back",
    "13": "Shoulders",
    "14": "Calves",
}
workoutMuscles = {
    "1": "Biceps brachii",
    "2": "Anterior deltoid",
    "3": "Serratus anterior",
    "4": "Pectoralis major",
    "5": "Triceps brachii",
    "6": "Rectus abdominis",
    "7": "Gastrocnemius",
    "8": "Gluteus maximus",
    "9": "Trapezius",
    "10": "Quadriceps femoris",
    "11": "Biceps femoris",
    "12": "Latissimus dorsi",
    "13": "Brachialis",
    "14": "Obliquus externus abdominis",
    "15": "Soleus",
}
workoutEquipments = {
    "1": "Barbell",
    "2": "SZ-Bar",
    "3": "Dumbbell",
    "4": "Gym mat",
    "5": "Swiss Ball",
    "6": "Pull-up bar",
    "7": "none (bodyweight exercise)",
    "8": "Bench",
    "9": "Incline bench",
    "10": "Kettlebell",
}
workoutLicenses = {"1": "CC-BY-SA 3", "2": "CC-BY-SA 4", "3": "CC0"}

# convert a label into another form, based on a dict
def convertLabel(label, conversions):
    return conversions[str(label)]


# converts an iterable container of labels to another form, based on a dict
def convertLabels(labels, conversions):
    result = []
    for label in labels:
        result.append(convertLabel(label, conversions))
    return result


# removes HTML tags that look like '<this>' from a string
def removeHTMLTags(text):
    return re.sub(r"<.*?>", "", text)  # change anything inside <> to nothing


# order/sort results as requested
def orderRecipes(query, orderName, orderBy):
    if orderName:
        if orderName == "readyInMinutes":
            if orderBy:
                query = query.order_by(Recipe.readyInMinutes)
            else:
                query = query.order_by(Recipe.readyInMinutes.desc())
        if orderName == "calories":
            if orderBy:
                query = query.order_by(
                    Recipe.nutrition["nutrients"][0]["amount"].as_float()
                )
            else:
                query = query.order_by(
                    Recipe.nutrition["nutrients"][0]["amount"].as_float().desc()
                )
        if orderName == "pricePerServing":
            if orderBy:
                query = query.order_by(Recipe.pricePerServing)
            else:
                query = query.order_by(Recipe.pricePerServing.desc())
        if orderName == "popularity":
            if orderBy:
                query = query.order_by(Recipe.aggregateLikes)
            else:
                query = query.order_by(Recipe.aggregateLikes.desc())
    return query


# filter recipes by specified parameters
def filterRecipes(
    query, cuisine, dietName, maxReadyTime, pricePerServing, maxCalories, minCalories
):
    if cuisine:
        query = query.filter(Recipe.cuisines.contains(cast([cuisine], ARRAY(String))))
    if dietName:
        query = query.filter(Recipe.diets.contains(cast([dietName], ARRAY(String))))
    if maxReadyTime:
        query = query.filter(Recipe.readyInMinutes <= maxReadyTime)
    query = query.filter(
        (Recipe.nutrition["nutrients"][0]["amount"].as_float() <= maxCalories)
        & (Recipe.nutrition["nutrients"][0]["amount"].as_float() >= minCalories)
    )
    if pricePerServing:
        query = query.filter(Recipe.pricePerServing <= pricePerServing)

    return query


def filterWorkouts(query, language, category, muscles, equipment):
    if language:
        query = query.filter(Workout.language.ilike(f"%{language}%"))
    if category:
        query = query.filter(Workout.category.ilike(f"%{category}%"))
    query = query.filter(
        Workout.muscles.contains(cast(muscles, ARRAY(String)))
        & Workout.equipment.contains(cast(equipment, ARRAY(String)))
    )
    return query


def orderWorkouts(query, ordering, order_by):
    # Ordering by equipment required/muscles targeted is not included
    # While the behavior is probably defined, it would not have much value
    # whitelist isn't a clean solution to prevent injection but it works
    # TODO phase 4 : whitelists and default values -> search.py?
    orderables = {"name", "category", "language", "id"}
    if order_by not in orderables:
        order_by = "id"
    directions = {"asc", "desc"}
    if ordering not in directions:
        ordering = "asc"

    if order_by:
        query = query.order_by(text(f"{order_by} {ordering}"))

    return query


def orderArticles(query, ordering, order_by):
    orderables = {"title", "author", "source", "id", "publishedAt"}
    if order_by not in orderables:
        order_by = "id"
    if order_by == "publishedAt":
        order_by = '"publishedAt"'  # quotes needed to order by this column
    directions = {"asc", "desc"}
    if ordering not in directions:
        ordering = "asc"
    query = query.order_by(text(f"{order_by} {ordering}"))
    return query


def filterArticles(query, source, publishedAt):
    if source:
        query = query.filter(Article.source.ilike(f"%{source}%"))
    if publishedAt:
        query = query.filter(Article.publishedAt.ilike(f"%{publishedAt}%"))
    return query
