import React from 'react'

/**
 * This makes sure that our pages show up only when the right path is used. 
 * If it is not one of the paths that we mentioned in App.tsx, then show NoMatch
 * on the page. 
 */

export const NoMatch = () => (
    <div>
        <h2>No Match</h2>  
    </div>

)