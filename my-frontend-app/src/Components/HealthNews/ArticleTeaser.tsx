import React, {Component} from 'react'
import {Row, Col, Card, CardDeck, 
    Button, Spinner} from 'react-bootstrap'
import axios from 'axios';
import Pagination from "react-pagination-bootstrap";
import {HighlighterText} from '../Highlight';



export class ArticleTeaser extends Component {
    state = {
        articles: [{
            id: "",
            urlToImage: "",
            title: "",
            author: "",
            source: "",
            description: ""
        }], 
        loading: true, 
    };
    currentPage: number = 1;
    queryParams: {} = {};
    stringQuery: string;

    componentWillReceiveProps = (props) => {
        this.queryParams = props.queryParams;
        this.getArticles();
    }
    constructor(props){
        super(props);
        this.stringQuery="";
    }

    generateQueryStrings = () => {
        var result: string[] = [];
        for (var key in this.queryParams) {
            result.push(key.concat("=").concat(this.queryParams[key]));
        }
        return result.join("&");
    }

    createStringQuery = () => {
        var result: string = "";
        for (var key in this.queryParams) {
            result += (this.queryParams[key]);
        } 
        return result;
    }

    getArticles = async () => {
        this.setState({loading: true});
        const pageString = '&page='.concat(this.currentPage.toString());
        const queryStrings = this.generateQueryStrings();
        this.stringQuery = this.createStringQuery();
        const searchUrl = 'https://api.healthathome.me/api/healthnews?'
            .concat(queryStrings).concat(pageString);
        const results = await axios.get(searchUrl);
        this.setState({articles: results.data, loading: false});
    };

    componentDidMount() {
        
        this.getArticles();
    }

    handlePageChange = (pageNumber) => {
        this.currentPage = pageNumber;
        this.getArticles();
    }

    render() {

        if (this.state.loading) {
            return (
                <Row className="justify-content-center">
                    <Spinner animation="border" role="status"/>
                    <h4>Loading...</h4>
                </Row>
            )
        }

        return (
            <Row className="justify-content-center">
                <CardDeck>
                    <Row>
                        {this.state.articles.map(article => (
                            <Col md={4} xs={12} className="my-3" 
                                key={article.id}>
                                <Card >
                                    <Card.Img variant="top" 
                                        src={article.urlToImage} 
                                        alt={article.urlToImage}/>
                                    <Card.Body>
                                        <Card.Title>
                                            <HighlighterText 
                                            text = {article.title}
                                            searchValue={this.stringQuery}/>
                                        </Card.Title>
                                        <Card.Subtitle>
                                            {article.author} - {article.source}
                                        </Card.Subtitle>
                                        <Card.Text>
                                            <HighlighterText 
                                            text = {article.description}
                                            searchValue={this.stringQuery}/>
                                        </Card.Text>
                                        <Button href={String("/healthnews/")
                                        .concat(article.id)}>Check out more...
                                        </Button>
                                    </Card.Body>
                                </Card>
                            </Col>
                        ))}
                        
                    </Row>
                </CardDeck>
                <Pagination
                    prevPageText='prev'
                    nextPageText='next'
                    firstPageText='first'
                    lastPageText='last'
                    activePage={this.currentPage}
                    itemsCountPerPage={10}
                    totalItemsCount={80}
                    pageRangeDisplayed={10}
                    onChange={this.handlePageChange}
                />
            </Row>
        )
    }

}