import React from 'react'
import {Button, Form, InputGroup } from 'react-bootstrap'


//searching component on the workouts page
export const WorkoutsSearch = (props) => {
    return (
        <div>
        <Form onSubmit={props.submitFunc}>
                <InputGroup>
                <Form.Control onChange={props.searchFunc} name="name" 
                    type="text" placeholder="Search for workouts"/>
                <InputGroup.Append>
                    <Button type="submit">Search</Button>
                </InputGroup.Append>
                </InputGroup>
        </Form>
        <br></br>
        
        </div>

    )
}