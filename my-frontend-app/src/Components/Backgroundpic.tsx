import React from 'react';
import { Jumbotron as Jumbo, Container} from 'react-bootstrap';
import styled from 'styled-components';
import healthImage from '../references/health.jpeg';

        /**
         * creating a constant style for all jumbotrons. 
         * they should all have all the components mentioned below
         * we might need to change this later to make each page diff???
         */

const Styles  = styled.div`
    .jumbo {
        background: url(${healthImage}) no-repeat fixed bottom;
        background-size: cover;
        color: #efefef;
        height: 200px;
        position: relative;
        z-index: -5;
    }

    .overlay{
        background-color: #000;
        opacity: 0.5;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        z-index: -3;
    }
`;

export const Jumbotron = () => (
    <Styles>
        <Jumbo fluid className ="jumbo">
            <div className="overlay"></div>
            <Container>
            
            </Container>
        </Jumbo>
    </Styles>
)
