import React, {Component} from 'react'
import {PostsRecipes} from './Components/Posts_Recipes';
import {Button} from 'react-bootstrap';
import ButtonToolbar from 'react-bootstrap/esm/ButtonToolbar'
import { Link } from 'react-router-dom'
import { RecipesSearch } from './Components/Recipes/RecipesSearch';


export class Recipes extends Component {
        //initialize query params in this state
        state = {
            queryParams: {}
        };

        searchSubmitFunc = (e) =>{
            e.preventDefault();
            this.setState({});
        }

        searchValueChanged = (e) => {
            this.state.queryParams[e.target.name] = 
            e.target.value;
        }

        render() {
            const queryParams :any = this.state.queryParams;
            const ssf = this.searchSubmitFunc.bind(this);
            const svc = this.searchValueChanged.bind(this);

            return (
                <div >
                    {/* <Container> */}
                    {/* header for recipes page */}
                    <h1 className = "my-5 text-primary text-center">Recipes</h1>
                    <hr></hr>
                    <h4>Take your pick of recipes that are not only healthy 
                        but also DELICIOUS.</h4>
                    <p>Find foods that you love, guilt-free snacking 
                        guaranteed!</p>
                
                    <hr></hr>
                    <RecipesSearch submitFunc = {ssf} searchFunc = {svc}/>
                
                    <hr></hr>
                    {/* Adding all the instances of recipes in dynamic
                    table here*/}
                    <PostsRecipes {...{queryParams: queryParams}}/> 

                    {/* Buttons that link to other model pages*/}
                    <hr></hr>
                    <hr></hr>
                    <Link to= '/workouts'> 
                        <ButtonToolbar>
                            <Button variant='light' size='sm' block>
                                Go to the Workouts page
                            </Button>
                        </ButtonToolbar>
                    </Link>
                    <Link to= '/healthnews'> 
                        <ButtonToolbar>
                            <Button variant='light' size='sm' block>
                                Go to the Health News page
                            </Button>
                        </ButtonToolbar>
                    </Link>
                    </div>
                )
            }

        }
   
