import React from 'react'
import {Row, Container, Card, CardDeck} from 'react-bootstrap'
import { GitStats } from './Components/About/GitStats'

const reactBootstrap = require('./Images/react-bootstrap-logo.png');
const aws = require('./Images/aws-logo.png');
const gitlab = require('./Images/gitlab-logo.png');
const postman = require('./Images/postman-logo.png');


export const About = () => (
    <div>
    <h2 className="text-center">Our Mission</h2>
        <hr></hr>  
        {/* website goals and intended users */}
        <p>Our goal is to help people fulfill their health and wellness goals.  
            Our website shows different healthy food items that can be used to 
            create meals, provides different exercises to incorporate in your 
            daily life, and gives users access to current health-related 
            news so they can make better lifestyle decisions. </p>
​
        <p>The intended users for this website are people that want get started 
            or continue to stay health and are looking for a one-stop shop for 
            affordable and effective lifestyle choices! By incorporating 
            recipes, workouts, and health-realted news, 
            our platform allows usersto stay motivated and engaged in 
            their wellness/fitness journey.</p>
    
        <br></br>
        <Container>
            <h3>Our Data Sources</h3>
            <p>Our data was collected for Workouts,
                 Health News, Recipes from the 
                following sources respectively: 
                <a href="https://wger.de/en/software/api">
                Workout Manager</a>,
            <a href="https://newsapi.org/"> News</a>, and  
            <a href="https://spoonacular.com/food-api">
                Spoonacular</a>.</p>
        </Container>
        <br></br>

        <hr/>
        <GitStats/>

        <Container>
            <h3>Tools and Documentation</h3>
            <CardDeck>
                <Row>
                    <Card>
                    <Card.Img variant="top" src= {reactBootstrap}/>
                        <Card.Body>
                            <Card.Title>React Bootstrap</Card.Title>
                            <Card.Text>A front-end framework that provides 
                                tools and methods to help setup our website
                                </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card>
                        <Card.Img variant="top" src= {aws}/>
                        <Card.Body>
                            <Card.Title>AWS Amplify</Card.Title>
                            <Card.Text>It allowed us to deploy and manage our
                                 full stack application.</Card.Text>
                        </Card.Body>
                    </Card>
                    <Card>
                        <Card.Img variant="top" src= {gitlab}/>
                        <Card.Body>
                            <Card.Title>GitLab</Card.Title>
                            <Card.Text>A tool that provides issue tracking and 
                                continuous integration services.
                            <a href=
                    "https://gitlab.com/health-at-home-team/health-at-home"> 
                                Our Gitlab Repo</a></Card.Text>
                        </Card.Body>
                    </Card>
                    <Card>
                        <Card.Img variant="top" src= {postman}/>
                        <Card.Body>
                            <Card.Title>Postman</Card.Title>
                            <Card.Text>We used Postman to develop and design 
                                our APIs. 
                            <a href=
                    "https://documenter.getpostman.com/view/12877376/TVRdAWsd">
                                     Our Postman Documentation</a></Card.Text>
                        </Card.Body>
                    </Card>
                </Row>
            </CardDeck>
​
        </Container> 
    </div>
    
)