import React, { Component} from 'react'
import { Button } from 'react-bootstrap';
import {Posts} from './Components/PostsWorkouts';
import { WorkoutsSearch } from './Components/Workouts/WorkoutsSearch';


export class Workouts1 extends Component {
    state = {
        queryParams: {}
    };
    searchKey;
    searchVal;

    searchSubmitFunc = (e) =>{
        e.preventDefault();
        this.setState({});
    }

    searchValueChanged = (e) => {
        this.state.queryParams[e.target.name] = e.target.value;
    }

        render() {

            const queryParams :any = this.state.queryParams;
            const ssf = this.searchSubmitFunc.bind(this);
            const svc = this.searchValueChanged.bind(this);

            return (
                <div className="container">
                    <h1 className = "my-5 text-primary text-center">
                        Workouts</h1>
                    <h4>Find any workout to incorporate into your everyday 
                        lifestyle!</h4>
                    <hr></hr>
                    <WorkoutsSearch submitFunc = {ssf} searchFunc = {svc}/>
                    <Posts {...{queryParams: queryParams}}/>
                    <hr></hr>
                    {/* Buttons that link to other model pages*/}
                    <Button variant="Primary" className="bg-primary text-white" 
                    href={'/recipes'}>View Recipes!</Button>
                    <Button variant="Primary" className="bg-primary text-white" 
                    href={'/healthnews'}>View Health News!</Button>
                </div>
            )
        }

    }
