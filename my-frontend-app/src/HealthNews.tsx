import React, {Component} from 'react'
import { Button, Card, Container, Row } from 'react-bootstrap'
import { ArticleTeaser } from './Components/HealthNews/ArticleTeaser'
import ButtonToolbar from 'react-bootstrap/esm/ButtonToolbar'
import { Link } from 'react-router-dom'
import { HealthNewsSearch } from './Components/HealthNews/HealthNewsSearch';


const workoutsInfo = "https://www.hhs.gov/fitness/resource-center/"
    .concat("physical-activity-resources/index.html");

export class HealthNews extends Component {
    state = {
        queryParams: {}
    };
    searchKey;
    searchVal;

    searchSubmitFunc = (e) =>{
        e.preventDefault();
        this.setState({});
    }

    searchValueChanged = (e) => {
        this.state.queryParams[e.target.name] = e.target.value;
    }

    render() {
        const queryParams :any = this.state.queryParams;
        const ssf = this.searchSubmitFunc.bind(this);
        const svc = this.searchValueChanged.bind(this);

        return (
            <div>
                <Container>
                    {/* Health News title row */}
                    <h2>Health News</h2>
                    <h4>Get the latest health news to stay 
                        up to date with the best health 
                        practices and information!</h4>
                    <h4>Additional health resources:</h4>
                    <p>Nutrition/Food Resources:</p>
                    <ul>
                        <p><Card.Link 
                            href="https://www.nutrition.gov/topics/whats-food">
                            nutrition.gov</Card.Link></p>
                        <p><Card.Link 
                            href="https://health.gov/our-work/food-nutrition">
                            health.gov</Card.Link></p>
                        <p><Card.Link 
                            href="https://foodandnutrition.org/">
                            foodandnutrition.gov</Card.Link></p>
                    </ul>
                    <p>Workout Resources:</p>
                    <ul>
                        <p><Card.Link href={workoutsInfo}>
                            hhs.gov</Card.Link></p>
                    </ul>
                    <p>General Health Resources:</p>
                    <ul>
                        <p><Card.Link 
                            href="https://www.usa.gov/health-resources">
                            usa.gov/health-resources</Card.Link></p>
                        <p><Card.Link 
                            href="https://www.texashealth.org/en">
                            texashealth.org</Card.Link></p>
                    </ul>
                    <hr/>
                    <HealthNewsSearch submitFunc = {ssf} searchFunc = {svc}/>
                
                    {/* Adding another row with vertical padding and 
                        internal components centered */}
                    <Row className="my-3 justify-content-center">
                        <ArticleTeaser {...{queryParams: queryParams}}/>
                    
                    </Row>
                </Container>
                <hr></hr>
                <hr></hr>
                <Link to= '/recipes'> 
                    <ButtonToolbar>
                        <Button variant='light' size='sm' block>
                            Go to the Recipes page
                        </Button>
                    </ButtonToolbar>
                </Link>
                <Link to= '/workouts'> 
                    <ButtonToolbar>
                        <Button variant='light' size='sm' block>
                            Go to the Workouts page
                        </Button>
                    </ButtonToolbar>
                </Link>
            </div>
        )
    }
}